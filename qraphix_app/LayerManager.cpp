#include "LayerManager.hpp"
#include "QraphicsScene.hpp"
#include "LayerModel.hpp"
#include "LayerWidget.hpp"
#include "Layer.hpp"
#include <QItemSelectionModel>

LayerManager::LayerManager(QraphicsScene *scene) :
    QObject(scene),
    m_model(nullptr),
    m_selectionModel(nullptr),
    m_current(nullptr),
    m_currentIndex(-1)
{
    m_model = new LayerModel(scene, this);
    m_selectionModel = new QItemSelectionModel(m_model, this);
    connect(m_selectionModel, SIGNAL(currentChanged(QModelIndex,QModelIndex)),
            this, SLOT(currentChanged(QModelIndex, QModelIndex)));
}

Layer *LayerManager::currentLayer() const
{
    return m_current;
}

int LayerManager::currentLayerIndex() const
{
    return m_currentIndex;
}

int LayerManager::layerCount() const
{
    return m_model->layerCount();
}

Layer *LayerManager::layerAt(int i) const
{
    return m_model->layerAt(i);
}

void LayerManager::addItem(QraphicsItem *item, int index)
{
    Layer   *layer = layerAt(index);
    if (layer && item)
        layer->add(item);
}

void LayerManager::removeItem(QraphicsItem *item)
{
    Layer   *layer;

    for (int i = 0;i < m_model->layerCount(); ++i)
    {
        layer = m_model->layerAt(i);
        if (layer->remove(item))
            break;
    }
}

LayerModel *LayerManager::model() const
{
    return m_model;
}

QItemSelectionModel *LayerManager::selectionModel() const
{
    return m_selectionModel;
}

bool LayerManager::hasCurrentLayer() const
{
    return (m_currentIndex >= 0 && m_currentIndex < m_model->layerCount());
}

void LayerManager::setCurrentLayer(int index)
{
    if (m_currentIndex != index)
    {
        if (index >= 0 && index < m_model->layerCount())
            m_current = m_model->layerAt(index);
        else
            m_current = nullptr;
         m_currentIndex = index;
    }
    emit currentLayerChanged(m_currentIndex, m_model->layerCount());
}

void LayerManager::moveCurrentLayerUp()
{
    int const   index = m_currentIndex - 1;

    if (m_model->moveLayerUp(m_currentIndex))
    {
        m_selectionModel->setCurrentIndex(m_model->index(index), QItemSelectionModel::ClearAndSelect);
        setCurrentLayer(index);
    }
}

void LayerManager::moveCurrentLayerDown()
{
    int const   index = m_currentIndex + 1;

    if (m_model->moveLayerDown(m_currentIndex))
    {
        m_selectionModel->setCurrentIndex(m_model->index(index), QItemSelectionModel::ClearAndSelect);
        setCurrentLayer(index);
    }
}

void LayerManager::createLayer(QString const &name, int index)
{
    if (m_model->createLayer(name, index))
    {
        m_selectionModel->setCurrentIndex(m_model->index(index), QItemSelectionModel::ClearAndSelect);
        setCurrentLayer(index);
    }
}

void LayerManager::destroyLayer(int pos)
{
    int     new_pos = -1;

    if (pos == m_currentIndex && m_model->layerCount() > 1)
    {
        if (pos < m_model->layerCount() - 1)
            new_pos = pos;
        else
            new_pos = m_model->layerCount() - 2;
    }
    m_model->destroyLayer(pos);
    if (m_model->layerCount() > 0)
    {
        m_selectionModel->setCurrentIndex(m_model->index(new_pos), QItemSelectionModel::ClearAndSelect);
        setCurrentLayer(new_pos);
    }
    else
    {
        setCurrentLayer(-1);
    }
}

void LayerManager::destroyLayers()
{
    m_model->destroyAllLayer();
    setCurrentLayer(-1);
}

void LayerManager::currentChanged(const QModelIndex &current, const QModelIndex &old)
{
    if (current.isValid())
        setCurrentLayer(current.row());
    Q_UNUSED(old)
}
