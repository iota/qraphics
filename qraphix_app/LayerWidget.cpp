#include "LayerWidget.hpp"
#include "LayerManager.hpp"
#include "LayerModel.hpp"
#include "QraphicsScene.hpp"
#include "LayoutHelper.hpp"
#include <QAction>
#include <QGridLayout>
#include <QListView>
#include <QToolButton>
#include <QItemSelectionModel>
#include <QInputDialog>

LayerWidget::LayerWidget(QWidget *parent) :
    QWidget(parent),
    m_view(nullptr),
    m_createNewLayer(nullptr),
    m_removeCurrentLayer(nullptr),
    m_moveUpCurrentLayer(nullptr),
    m_moveDownCurrentLayer(nullptr)
{
    QGridLayout *layout = new QGridLayout(this);
    QWidget     *buttonPanel = new QWidget;

    m_view = new QListView;
    m_createNewLayer = new QAction(QIcon(":/images/images/layer_create.png"), tr("Create layer..."), this);
    m_removeCurrentLayer = new QAction(QIcon(":/images/images/layer_destroy.png"), tr("Remove layer"), this);
    m_moveUpCurrentLayer = new QAction(QIcon(":/images/images/arrow-090.png"), tr("Move up"), this);
    m_moveDownCurrentLayer = new QAction(QIcon(":/images/images/arrow-270.png"), tr("Move down"), this);
    setupButtons(buttonPanel);
    layout->addWidget(m_view);
    layout->addWidget(buttonPanel, 0, 1);
    utils::setupLayout(layout);
}

void LayerWidget::setupButtons(QWidget *widget)
{
    QVBoxLayout *layout = new QVBoxLayout(widget);
    QToolButton *add = new QToolButton;
    QToolButton *remove = new QToolButton;
    QToolButton *moveUp = new QToolButton;
    QToolButton *moveDown = new QToolButton;

    add->setDefaultAction(m_createNewLayer);
    remove->setDefaultAction(m_removeCurrentLayer);
    moveUp->setDefaultAction(m_moveUpCurrentLayer);
    moveDown->setDefaultAction(m_moveDownCurrentLayer);
    layout->addSpacerItem(new QSpacerItem(0, 200, QSizePolicy::Expanding, QSizePolicy::Expanding));
    layout->addWidget(add);
    layout->addWidget(remove);
    layout->addWidget(moveUp);
    layout->addWidget(moveDown);
    layout->addSpacerItem(new QSpacerItem(0, 200, QSizePolicy::Expanding, QSizePolicy::Expanding));
    connect(m_createNewLayer, SIGNAL(triggered()),this,SLOT(createNewLayerImp()), Qt::DirectConnection);
    connect(m_removeCurrentLayer,SIGNAL(triggered()), this, SIGNAL(destroyLayer()), Qt::DirectConnection);
    connect(m_moveUpCurrentLayer,SIGNAL(triggered()), this, SIGNAL(moveLayerUp()), Qt::DirectConnection);
    connect(m_moveDownCurrentLayer,SIGNAL(triggered()), this, SIGNAL(moveLayerDown()), Qt::DirectConnection);
}

void LayerWidget::setModel(LayerModel *model)
{
    m_view->setModel(model);
}

void LayerWidget::setSelectionModel(QItemSelectionModel *selectionModel)
{
    m_view->setSelectionModel(selectionModel);
    m_view->setCurrentIndex(selectionModel->model()->index(0, 0));
}

void LayerWidget::selectionChanged(int layerIndex, int layerCount)
{
    bool const  hasLayers = layerCount > 0;

    m_removeCurrentLayer->setEnabled(hasLayers && layerCount > 1);
    m_moveUpCurrentLayer->setEnabled(hasLayers && layerIndex > 0 && layerCount > 1);
    m_moveDownCurrentLayer->setEnabled(hasLayers && layerCount > 1 && layerIndex + 1 < layerCount);
}

void LayerWidget::createNewLayerImp()
{
    bool    ok;
    QString layerName = QInputDialog::getText(this,
                                              tr("Create new layer:"),
                                              tr("Layer name:"),
                                              QLineEdit::Normal,
                                              "",
                                              &ok);
    layerName = layerName.trimmed();
    if (ok && layerName.isEmpty() == false)
        emit createLayer(layerName);
}
