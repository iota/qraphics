#ifndef TOOLMANAGER_HPP
#define TOOLMANAGER_HPP
#include <QObject>
#include <memory>

class QraphicsScene;
class ToolModel;
class ToolFactory;
class AbstractTool;
class QGraphicsSceneMouseEvent;

class ToolManager : public QObject
{
public:
    enum  : int
    {
        TOOL_POINTER = 0,
        TOOL_LINE,
        TOOL_PLINE,
        TOOL_COUNT
    };

    ToolManager(QraphicsScene *parent);
    ~ToolManager();

    ToolModel *model()const;
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);
    void changeTool(int index);
private:
    void setTool(AbstractTool *tool);
private:
    QraphicsScene                   *m_scene;
    ToolFactory                     *m_toolFactory;
    std::unique_ptr<AbstractTool>   m_currentTool;
};



#endif // TOOLMANAGER_HPP
