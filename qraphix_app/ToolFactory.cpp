#include "ToolFactory.hpp"
#include "AbstractTool.hpp"

AbstractTool *ToolFactory::create(int key, QObject *parent) const
{
    ToolCreator creator = m_creators.value(key);

    if (creator)
    {
        return creator(parent);
    }
    else
    {
        return nullptr;
    }
}

ToolFactory::ToolFactory(QObject *parent) :
    QObject(parent),
    m_toolModel(nullptr)
{
    m_toolModel = new ToolModel(this);
}


ToolModel *ToolFactory::getModel() const
{
    return m_toolModel;
}
