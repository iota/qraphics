#include "PolyLineTool.hpp"
#include "QraphicsPolyLine.hpp"
#include "QraphicsLine.hpp"
#include "QraphicsScene.hpp"
#include <QGraphicsSceneMouseEvent>

PolyLineTool::PolyLineTool(QObject *parent) :
    QObject(parent),
    m_newPoly(nullptr)
{
}

void PolyLineTool::mouseDoubleClick(QGraphicsSceneMouseEvent *event, QraphicsScene *scene)
{
    if (m_newPoly)
    {
        if (event->button() & Qt::LeftButton)
        {
            m_newPoly->setLastPoint(event->scenePos());
            m_newPoly->setPen(scene->currentPen());
            m_newPoly->setBoxesVisible(false);
            m_newPoly->setHandlesVisible(false);
            m_newPoly = nullptr;
        }
        else
        {
            scene->destroyQphxItem(m_newPoly);
            m_newPoly = nullptr;
        }
        scene->update();
    }
}

void PolyLineTool::mousePress(QGraphicsSceneMouseEvent *event, QraphicsScene *scene)
{
    if (m_newPoly)
    {
        if (event->button() & Qt::LeftButton)
        {
            m_newPoly->addPoint(event->scenePos());
        }
        else if (event->button() & Qt::RightButton)
        {
            m_newPoly->removeLastPoint();
            if (m_newPoly->pointCount() > 1)
            {
                m_newPoly->setPen(scene->currentPen());
                m_newPoly->setBoxesVisible(false);
                m_newPoly->setHandlesVisible(false);
            }
            else
            {
                scene->destroyQphxItem(m_newPoly);
            }
            m_newPoly = nullptr;
            scene->update();
            return;
        }
    }
    else if (event->button() & Qt::LeftButton)
    {
        QVector<QPointF>    points;

        points << event->scenePos();
        points << event->scenePos();
        m_newPoly = scene->addQphxPolyLine(points, scene->ghostPen());
        m_newPoly->setBoxesVisible(true);
        m_newPoly->setHandlesVisible(true);
        m_newPoly->update();
    }
}

void PolyLineTool::mouseRelease(QGraphicsSceneMouseEvent *event, QraphicsScene *scene)
{
    Q_UNUSED(event)
    Q_UNUSED(scene)
}

void PolyLineTool::mouseMove(QGraphicsSceneMouseEvent *event, QraphicsScene *scene)
{
    if (m_newPoly && m_newPoly->pointCount() > 1)
    {
        m_newPoly->setLastPoint(event->scenePos());
        scene->update();
    }
    Q_UNUSED(scene)
}

void PolyLineTool::begin(QraphicsScene *scene)
{
    Q_UNUSED(scene)
}

void PolyLineTool::end(QraphicsScene *scene)
{
    if (m_newPoly)
    {
        scene->destroyQphxItem(m_newPoly);
        m_newPoly = nullptr;
    }
    scene->update();
}