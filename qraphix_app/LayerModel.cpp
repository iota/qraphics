#include "LayerModel.hpp"
#include "Layer.hpp"
#include <QVariant>

LayerModel::LayerModel(QraphicsScene *scene, QObject *parent) :
    QAbstractListModel(parent),
    m_scene(scene)
{
}

Layer *LayerModel::createLayer(const QString &name, int index)
{
    Layer   *layer = new Layer(name, m_scene, index, this);

    beginInsertRows(QModelIndex(), index, index);
    m_layers.insert(index, layer);
    endInsertRows();
    return layer;
}

void LayerModel::destroyLayer(int index)
{
    if (index >= 0 && index < m_layers.size())
    {
        beginRemoveRows(QModelIndex(), index, index);
        delete m_layers[index];
        m_layers.removeAt(index);
        endRemoveRows();
    }
}

void LayerModel::destroyAllLayer()
{
    beginResetModel();
    for (Layer *layer : m_layers)
        delete layer;
    m_layers.clear();
    endResetModel();
}

bool LayerModel::moveLayerUp(int index)
{
    int     sourceOrder;
    int     destOrder;
    bool    ok = false;
    int     destIndex = index - 1;

    if (destIndex >= 0)
    {
        beginMoveRows(QModelIndex(),
                          index,
                          index,
                          QModelIndex(),
                          destIndex);
        sourceOrder = m_layers.at(index)->getOrder();
        destOrder = m_layers.at(destIndex)->getOrder();
        m_layers.at(index)->setOrder(destOrder);
        m_layers.at(destIndex)->setOrder(sourceOrder);
        m_layers.move(index, destIndex);
        ok = true;
        endMoveRows();
    }
    return ok;
}

bool LayerModel::moveLayerDown(int index)
{
    int     sourceOrder;
    int     destOrder;
    bool    ok = false;
    int     destIndex = index + 1;

    if (m_layers.count() > 1 && destIndex < m_layers.count())
    {
        beginMoveRows(QModelIndex(),
                      index,
                      index,
                      QModelIndex(),
                      destIndex + 1);
        sourceOrder = m_layers.at(index)->getOrder();
        destOrder = m_layers.at(destIndex)->getOrder();
        m_layers.at(index)->setOrder(destOrder);
        m_layers.at(destIndex)->setOrder(sourceOrder);
        m_layers.move(index, destIndex);
        ok = true;
        endMoveRows();
    }
    return ok;
}

int LayerModel::layerCount() const
{
    return m_layers.size();
}

int LayerModel::rowCount(const QModelIndex &) const
{
    return layerCount();
}

QVariant LayerModel::data(const QModelIndex &index, int role) const
{
    QVariant    result;

    if (index.isValid())
    {
        switch(role)
        {
        case Qt::DisplayRole:
        case Qt::EditRole:
            result = m_layers.at(index.row())->name();
            break;
        case Qt::CheckStateRole:
            result = (m_layers.at(index.row())->isVisible()) ? Qt::Checked : Qt::Unchecked;
            break;
        default:
            break;
        }
    }
    return result;
}

Layer *LayerModel::layerAt(int i) const
{
    Layer *layer = nullptr;

    layer = m_layers.at(i);
    return (layer);
}

Qt::ItemFlags LayerModel::flags(const QModelIndex &index) const
{
    if (index.isValid())
        return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsUserCheckable;
    else
        return QAbstractListModel::flags(index);
}

bool LayerModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    bool    ok = false;

    if (index.isValid())
    {
        switch(role)
        {
        case Qt::DisplayRole:
        case Qt::EditRole:
            m_layers.at(index.row())->setName(value.toString());
            ok = true;
            break;
        case Qt::CheckStateRole:
            m_layers.at(index.row())->setVisible((value.value<int>() == Qt::Checked));
            ok = true;
            break;
        default:
            break;
        }
        if (ok)
            emit dataChanged(index, index);
    }
    return ok;
}