#include "LineTool.hpp"
#include "QraphicsLine.hpp"
#include "QraphicsScene.hpp"
#include <QGraphicsSceneMouseEvent>

LineTool::LineTool(QObject *parent) :
    QObject(parent),
    m_newLine(nullptr)
{
}

void LineTool::mouseDoubleClick(QGraphicsSceneMouseEvent *event, QraphicsScene *scene)
{
    Q_UNUSED(event)
    Q_UNUSED(scene)
}

void LineTool::mousePress(QGraphicsSceneMouseEvent *event, QraphicsScene *scene)
{
    if (m_newLine)
    {
        if (event->button() & Qt::LeftButton)
        {
            m_newLine->setP1(event->scenePos());
            m_newLine->setPen(scene->currentPen());
            m_newLine->setBoxesVisible(false);
            m_newLine->setHandlesVisible(false);
            m_newLine = nullptr;
        }
        else
        {
            scene->destroyQphxItem(m_newLine);
            m_newLine = nullptr;
        }
        scene->update();
    }
    else if (event->button() & Qt::LeftButton)
    {
        m_newLine = scene->addQphxLine(QLineF(event->scenePos(), event->scenePos()), scene->ghostPen());
        m_newLine->setBoxesVisible(true);
        m_newLine->setHandlesVisible(true);
        scene->update();
    }
}

void LineTool::mouseRelease(QGraphicsSceneMouseEvent *event, QraphicsScene *scene)
{
    Q_UNUSED(event)
    Q_UNUSED(scene)
}

void LineTool::mouseMove(QGraphicsSceneMouseEvent *event, QraphicsScene *scene)
{
    if (m_newLine)
    {
        m_newLine->setP1(event->scenePos());
        scene->update();
    }
}

void LineTool::begin(QraphicsScene *scene)
{
    m_newLine = nullptr;
    scene->update();
    Q_UNUSED(scene)
}

void LineTool::end(QraphicsScene *scene)
{
    if (m_newLine)
    {
        scene->destroyQphxItem(m_newLine);
        m_newLine = nullptr;
    }
    scene->update();
}