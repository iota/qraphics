#ifndef LAYERMODEL_HPP
#define LAYERMODEL_HPP
#include <QAbstractListModel>

class Layer;
class QraphicsScene;
class QString;

class LayerModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit LayerModel(QraphicsScene *scene, QObject *parent);

    Layer			*createLayer(QString const &name, int index);
    void			destroyLayer(int index);
    void			destroyAllLayer();
    bool			moveLayerUp(int index);
    bool			moveLayerDown(int index);
    int				layerCount()const;
    int				currentIndex()const;
    Layer			*layerAt(int i)const;

    int             rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant        data(const QModelIndex &index, int role) const;
    bool            setData(const QModelIndex &index,
                            const QVariant &value,
                            int role);
    Qt::ItemFlags   flags(const QModelIndex &index) const;
private:
    QList<Layer *>	m_layers;
    QraphicsScene	*m_scene;
};

#endif // LAYERMODEL_HPP
