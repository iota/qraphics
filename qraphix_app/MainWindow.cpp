#include "MainWindow.hpp"
#include "ActionModel.hpp"
#include "QraphicsScene.hpp"
#include "QraphicsView.hpp"
#include "LineWidget.hpp"
#include "LayerWidget.hpp"
#include "LayoutHelper.hpp"
#include "ToolWidget.hpp"
#include <QGridLayout>
#include <QListView>
#include <QAction>
#include <QDockWidget>
#include <QGraphicsProxyWidget>
#include <QVBoxLayout>
#include <QSettings>
#include <QInputDialog>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    m_view(nullptr),
    m_penWidget(nullptr),
    m_toolWidget(nullptr),
    m_layerWidget(nullptr)
{
    QWidget *internal = new QWidget(this);
    QGridLayout *layout = new QGridLayout(internal);
    QSettings   settings;

    m_view = new QraphicsView(this);
    m_scene = new QraphicsScene(this);

    layout->addWidget(m_view, 0, 0);
    utils::setupLayout(layout);
    setCentralWidget(m_view);
    setupDockWidgets();

    m_view->setScene(m_scene);
    m_view->setSceneRect(settings.value("init_scene_rect", QRectF(-4000, -3000, 8000, 6000)).toRectF());
    m_scene->setup(m_toolWidget, m_penWidget, m_layerWidget);

    loadSettings(settings);
}

MainWindow::~MainWindow()
{
}

void MainWindow::loadSettings(const QSettings &settings)
{
    if (restoreState(settings.value("states").toByteArray()) == false)
        qWarning("Restore state failed");
    if (restoreGeometry(settings.value("geometry").toByteArray()) == false)
        qWarning("Restore geometry failed");
}

void MainWindow::saveSettings(QSettings &settings) const
{
    settings.setValue("geometry", saveGeometry());
    settings.setValue("states", saveState());
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    QSettings   settings;

    saveSettings(settings);
    QMainWindow::closeEvent(event);
}

void MainWindow::setupDockWidgets()
{
    QDockWidget *penDock = new QDockWidget(tr("Lines"), this);
    QDockWidget *layerDock = new QDockWidget(tr("Layers"), this);

    m_penWidget = new LineWidget(this);
    m_layerWidget = new LayerWidget(this);
    penDock->setWidget(m_penWidget);
    penDock->setObjectName("pen_dock");
    layerDock->setWidget(m_layerWidget);
    layerDock->setObjectName("layer_dock");
    addDockWidget(Qt::RightDockWidgetArea, penDock);
    addDockWidget(Qt::RightDockWidgetArea, layerDock);

    m_toolWidget = new ToolWidget(tr("Tools"), this);
    addToolBar(Qt::TopToolBarArea, m_toolWidget);
}
