#-------------------------------------------------
#
# Project created by QtCreator 2014-05-07T03:01:43
#
#-------------------------------------------------

QT				+= core gui widgets
QMAKE_CXXFLAGS	+= -std=c++11
TARGET			= qraphix_app
TEMPLATE		= app

# Necessaire pour pouvoir supporter le C++11 sous mac avec
# le compilateur Clang.
macx
{
    QMAKE_CXXFLAGS += -stdlib=libc++
    QMAKE_CXXFLAGS += -mmacosx-version-min=10.7
    QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.7
}

SOURCES += main.cpp \
	MainWindow.cpp \
	QraphicsScene.cpp \
	QraphicsView.cpp \
	ActionModel.cpp \
	LineTool.cpp \
	AbstractTool.cpp \
	ToolWidget.cpp \
	ToolModel.cpp \
	ToolFactory.cpp \
	QraphicsLine.cpp \
	LineWidget.cpp \
	PointerTool.cpp \
	QraphicsItem.cpp \
	QraphicsPolyLine.cpp \
	PolyLineTool.cpp \
	LayerWidget.cpp \
	LayerManager.cpp \
	LayoutHelper.cpp \
    ToolManager.cpp \
    Layer.cpp \
    LayerModel.cpp

HEADERS  += \
	MainWindow.hpp \
	QraphicsScene.hpp \
	QraphicsView.hpp \
	ActionModel.hpp \
	AbstractTool.hpp \
	LineTool.hpp \
	ToolWidget.hpp \
	ToolModel.hpp \
	ToolFactory.hpp \
	QraphicsLine.hpp \
	LineWidget.hpp \
	PointerTool.hpp \
	QraphicsItem.hpp \
	QraphicsPolyLine.hpp \
	PolyLineTool.hpp \
	CachedValue.hpp \
	LayerWidget.hpp \
	LayerManager.hpp \
	LayoutHelper.hpp \
    ToolManager.hpp \
    Layer.hpp \
    LayerModel.hpp

FORMS    += main_window.ui \
	LineWidget.ui

RESOURCES += \
	graphix.qrc
.
