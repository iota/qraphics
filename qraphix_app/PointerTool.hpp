#ifndef POINTERTOOL_HPP
#define POINTERTOOL_HPP
#include <QObject>
#include "AbstractTool.hpp"

class QraphicsAnchor;
class AbstractAnchorable;

class PointerTool : public QObject, public AbstractTool
{
    Q_OBJECT
public:
    explicit PointerTool(QObject *parent = 0);
public:
    void mouseDoubleClick(QGraphicsSceneMouseEvent *event, QraphicsScene *scene);
    void mousePress(QGraphicsSceneMouseEvent *event, QraphicsScene *scene);
    void mouseRelease(QGraphicsSceneMouseEvent *event, QraphicsScene *scene);
    void mouseMove(QGraphicsSceneMouseEvent *event, QraphicsScene *scene);
    void begin(QraphicsScene *scene);
    void end(QraphicsScene *scene);
private:
    bool m_press;
};

#endif // POINTERTOOL_HPP
