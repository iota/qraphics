#ifndef LINETOOL_HPP
#define LINETOOL_HPP
#include "AbstractTool.hpp"
#include <QPointF>
#include <QGraphicsLineItem>
#include <QObject>

class QraphicsLine;
class QPropertyAnimation;

class LineTool : public QObject, public AbstractTool
{
    Q_OBJECT
public:
    LineTool(QObject *parent);
public:
    void mouseDoubleClick(QGraphicsSceneMouseEvent *event, QraphicsScene *scene);
    void mousePress(QGraphicsSceneMouseEvent *event, QraphicsScene *scene);
    void mouseRelease(QGraphicsSceneMouseEvent *event, QraphicsScene *scene);
    void mouseMove(QGraphicsSceneMouseEvent *event, QraphicsScene *scene);
    void begin(QraphicsScene *scene);
    void end(QraphicsScene *scene);
public:
    QraphicsLine    *m_newLine;
};

#endif // LINETOOL_HPP
