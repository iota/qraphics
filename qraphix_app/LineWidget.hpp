#ifndef LINEWIDGET_HPP
#define LINEWIDGET_HPP

#include <QWidget>
#include <QMap>
#include <QPen>

namespace Ui
{
    class LineWidget;
}

class LineWidget : public QWidget
{
    Q_OBJECT

public:
    explicit LineWidget(QWidget *parent = 0);
    ~LineWidget();

    void setPen(const QPen &pen);
public slots:
    void	onSizeChanged(int value);
    void	onStyleChanged(int index);
    void	onCapChanged(int index);
    void	onJoinChanged(int index);
signals:
    void    penChanged(const QPen &pen);
private:
    Ui::LineWidget              *ui;
    QPen						m_pen;
    QMap<int, Qt::PenStyle>		m_styleMap;
    QMap<int, Qt::PenCapStyle>	m_capMap;
    QMap<int, Qt::PenJoinStyle>	m_joinMap;
};

#endif // LINEWIDGET_HPP
