#include "MainWindow.hpp"
#include <QApplication>
#include <QSettings>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setApplicationName("qraphics");
    app.setApplicationVersion("test");
    app.setApplicationDisplayName("qrphx");
    app.setOrganizationName("dead-dev-society");
    QSettings::setDefaultFormat(QSettings::IniFormat);
    MainWindow window;
    window.show();
    return app.exec();
}
