#ifndef QRAPHICSSCENE_HPP
#define QRAPHICSSCENE_HPP
#include <QGraphicsScene>
#include <QSet>

class QGraphicsSceneMouseEvent;
class ToolFactory;
class LayerManager;
class AbstractTool;
class LineWidget;
class ToolWidget;
class ToolManager;
class LayerWidget;
class QraphicsItem;
class QraphicsLine;
class QraphicsPolyLine;
class Layer;
class LayerModel;

class QraphicsScene : public QGraphicsScene
{
    Q_OBJECT

public:
    explicit QraphicsScene(QObject *parent = 0);
   ~QraphicsScene();

    void                addQphxItem(QraphicsItem *item);
    void                removeQphxItem(QraphicsItem *item);
    void                destroyQphxItem(QraphicsItem *item);
    QraphicsLine        *addQphxLine(const QLineF &line, const QPen &pen);
    QraphicsPolyLine    *addQphxPolyLine(const QVector<QPointF> &points, QPen const &pen);

    QPen const          &currentPen()const;
    QPen const          &ghostPen()const;
    bool                isCurrentLayerVisible()const;

    void                setup(ToolWidget *toolWidget, LineWidget *penWidget, LayerWidget *layerWidget);
    void                mousePressEvent(QGraphicsSceneMouseEvent *event);
    void                mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void                mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void                mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);
public slots:
    void                setTool(int index);
    void                setCurrentPen(QPen const &pen);
    void                setCurrentLayer(int index);
    void                createNewLayer(QString const &name);
    void                removeCurrentLayer();
    void                moveCurrentLayerUp();
    void                moveCurrentLayerDown();
private:
    QPen            m_currentPen;
    QPen            m_ghostPen;
    QBrush          m_brush;
    ToolManager     *m_toolManager;
    LayerManager    *m_layerManager;
};

#endif // QRAPHICSSCENE_HPP
