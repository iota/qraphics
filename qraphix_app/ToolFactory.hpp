#ifndef TOOLFACTORY_HPP
#define TOOLFACTORY_HPP
#include "ToolModel.hpp"
#include <QObject>
#include <QMap>
#include <functional>

class QObject;
class AbstractTool;

/*!
 * \brief   Fabrique d'outils
 *
 * Permet de construire l'ensemble des outils à partir d'une clé.
 * La méthode template registerTool() permet d'ajouter un nouveau type
 * (le parametre template) associé à une clé (le parametre de la methode).
 * L'interet principal est de faciliter l'ajout de nouvelle classe d'outil.
 */
class ToolFactory : public QObject
{
public:
    explicit ToolFactory(QObject *parent);

    template <class Tool>
    void    registerTool(QString const &label, const QIcon &icon, int key);

    AbstractTool    *create(int key, QObject *parent)const;
    ToolModel       *getModel()const;
private:
    typedef std::function<AbstractTool *(QObject *)> ToolCreator;

    QMap<int, ToolCreator>  m_creators;
    ToolModel               *m_toolModel;
};

template <class Tool>
void ToolFactory::registerTool(QString const &label, const QIcon &icon, int key)
{
    m_creators[key] = [](QObject *parent){return new Tool(parent);};
    m_toolModel->addTool(label, icon);
}

#endif // TOOLFACTORY_HPP
