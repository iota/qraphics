#include "LayoutHelper.hpp"
#include <QLayout>

namespace utils
{
    void    setupLayout(QLayout *layout)
    {
        layout->setSpacing(0);
        layout->setMargin(0);
        layout->setContentsMargins(0, 0, 0, 0);
    }
}