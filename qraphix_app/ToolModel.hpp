#ifndef TOOLMODEL_HPP
#define TOOLMODEL_HPP

#include <QAbstractListModel>
#include <QString>
#include <QIcon>
#include <QList>
class QString;
class QIcon;

class ToolModel : public QAbstractListModel
{
    Q_OBJECT

    struct ToolMetaData
    {
        QString label;
        QIcon   icon;
    };
public:
    explicit ToolModel(QObject *parent = 0);

    void addTool(const QString &label, const QIcon &icon);
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
private:
    QList<ToolMetaData> m_items;
};

#endif // TOOLMODEL_HPP
