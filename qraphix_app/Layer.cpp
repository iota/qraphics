#include "Layer.hpp"
#include "QraphicsItem.hpp"
#include "QraphicsScene.hpp"
#include <QGraphicsItemGroup>
#include <cmath>

Layer::Layer(const QString &name, QraphicsScene *scene, int id, QObject *parent) :
    QObject(parent),
    m_name(name),
    m_id(id),
    m_scene(scene),
    m_group(nullptr)
{
    m_group = m_scene->createItemGroup(QList<QGraphicsItem *>());
    m_group->setZValue(id);
}

Layer::~Layer()
{
    m_scene->destroyItemGroup(m_group);
    for (QraphicsItem *item : m_items)
    {
        m_scene->removeItem(item);
        delete item;
    }
    m_items.clear();
}

void Layer::setName(const QString &name)
{
    m_name = name;
}

void Layer::setVisible(bool visible)
{
    m_group->setVisible(visible);
}

void Layer::setOrder(int order)
{
    m_group->setZValue(order);
}

int Layer::getOrder() const
{
    return std::ceil(m_group->zValue());
}

void Layer::add(QraphicsItem *item)
{
    if (m_items.contains(item) == false)
    {
        m_group->addToGroup(item);
        m_items.insert(item);
    }
}

bool Layer::remove(QraphicsItem *item)
{
    auto    it = m_items.find(item);

    if (it != m_items.end())
    {
        m_group->removeFromGroup(item);
        m_scene->removeItem(item);
        m_items.erase(it);
        return true;
    }
    return false;
}

const QString &Layer::name() const
{
    return m_name;
}

bool Layer::isVisible() const
{
    return m_group->isVisible();
}