#ifndef QRAPHICSITEM_HPP
#define QRAPHICSITEM_HPP

#include <QGraphicsObject>

class QraphicsItem : public QGraphicsObject
{
public:
    QraphicsItem(QGraphicsItem *parent);

    void    setBoxesVisible(bool state);
    void    setHandlesVisible(bool state);
    int     layer()const;
    void    setLayerId(int id);

    virtual void paint(QPainter * p, const QStyleOptionGraphicsItem *style, QWidget *widget);
    virtual int pointCount()const = 0;
    virtual QPointF pointAt(int i)const = 0;
    virtual void setPointAt(int i, QPointF const &) = 0;
private:
    virtual void draw(QPainter * p, const QStyleOptionGraphicsItem *, QWidget *) = 0;
    void drawItemHandles(QPainter *p)const;
    void drawItemBox(QPainter *p) const;
protected:
    virtual void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    virtual void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);
private:
    bool    m_showBoxes;
    bool    m_showHandles;
};

namespace bbox
{
    void adjustBBox(QPen const & pen, QRectF &bbox);
}

#endif // QRAPHICSITEM_HPP
