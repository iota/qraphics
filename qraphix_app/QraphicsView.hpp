#ifndef QRAPHICS_VIEW_HPP
#define QRAPHICS_VIEW_HPP

#include <QGraphicsView>

class QraphicsView : public QGraphicsView
{
    Q_OBJECT
public:
    explicit QraphicsView(QWidget *parent = 0);
};

#endif // QRAPHICS_VIEW_HPP
