#ifndef CACHEDVALUE_HPP
#define CACHEDVALUE_HPP

/*!
 *  \class CachedValue
 *  \brief Gère une variable de stockage de resultat précalculé.
 */
template <class T>
class CachedValue
{
public:
    CachedValue(bool modified, const T &value) :
        m_value(value),
        m_modified(modified)
    {
    }

    CachedValue() :
        m_modified(true)
    {
    }

    bool needUpdate()const
    {
        return m_modified;
    }

    void setModified(bool state = true)
    {
        m_modified = state;
    }

    void update(const T &value)
    {
        m_value = value;
        m_modified = false;
    }

    const T get()const
    {
        return m_value;
    }
private:
    T       m_value;
    bool    m_modified;
};

#endif // CACHEDVALUE_HPP