#include "ActionModel.hpp"
#include <QtDebug>

ActionModel::ActionModel(QObject *parent) :
    QAbstractTableModel(parent)
{
}

void	ActionModel::add(QAction *action)
{
    QAbstractTableModel::beginInsertRows(QModelIndex(),
                                         m_actions.size(),
                                         1);
    m_actions.append(QSharedPointer<QAction>(action));
    QAbstractTableModel::endInsertRows();
}

int ActionModel::rowCount(const QModelIndex &parent) const
{
    return (m_actions.count());
    Q_UNUSED(parent)
}

int ActionModel::columnCount(const QModelIndex &parent) const
{
    return (ActionModel::ColumnCount);
    Q_UNUSED(parent)
}

QVariant ActionModel::data(const QModelIndex &index, int role) const
{
    QVariant	result;

    qDebug() << index.column() << " ; " << index.row();
    if (index.isValid() && (role == Qt::DisplayRole || role == Qt::EditRole || role == Qt::DecorationRole))
    {
        if (role == Qt::DecorationRole)
        {
            result = m_actions.at(index.row())->icon();
        }
        else
        {
            switch (index.column())
            {
            case ActionModel::Label:
                result = m_actions.at(index.row())->text();
                break;
            default:
                break;
            }
        }

    }
    return (result);
}
