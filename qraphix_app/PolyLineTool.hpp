#ifndef POLYLINETOOL_HPP
#define POLYLINETOOL_HPP
#include "AbstractTool.hpp"
#include <QPointF>
#include <QGraphicsLineItem>
#include <QObject>

class QraphicsPolyLine;
class QraphicsLine;
class QPropertyAnimation;

class PolyLineTool : public QObject, public AbstractTool
{
    Q_OBJECT
public:
    PolyLineTool(QObject *parent);
public:
    void mouseDoubleClick(QGraphicsSceneMouseEvent *event, QraphicsScene *scene);
    void mousePress(QGraphicsSceneMouseEvent *event, QraphicsScene *scene);
    void mouseRelease(QGraphicsSceneMouseEvent *event, QraphicsScene *scene);
    void mouseMove(QGraphicsSceneMouseEvent *event, QraphicsScene *scene);
    void begin(QraphicsScene *scene);
    void end(QraphicsScene *scene);
public:
    QraphicsPolyLine    *m_newPoly;
};

#endif // POLYLINETOOL_HPP
