#include "ToolModel.hpp"
#include <QString>
#include <QIcon>
#include <QVariant>

ToolModel::ToolModel(QObject *parent) :
    QAbstractListModel(parent)
{
}

void ToolModel::addTool(const QString &label, const QIcon &icon)
{
    ToolMetaData    item;

    beginInsertRows(QModelIndex(), m_items.count(), m_items.count());
    item.label = label;
    item.icon = icon;
    m_items.append(item);
    endInsertRows();
}

int ToolModel::rowCount(const QModelIndex &parent) const
{
    return m_items.count();
    Q_UNUSED(parent);
}

QVariant ToolModel::data(const QModelIndex &index, int role) const
{
    QVariant    result;
    ToolMetaData  item;

    if (index.isValid())
    {
        switch (role)
        {
        case Qt::DisplayRole:
        case Qt::EditRole:
            result = m_items.at(index.row()).label;
            break;
        case Qt::DecorationRole:
            result = m_items.at(index.row()).icon;
            break;
        default:
            break;
        }
    }
    return result;
}

