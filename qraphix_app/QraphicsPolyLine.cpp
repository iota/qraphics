#include "QraphicsPolyLine.hpp"
#include <QPainter>
#include <QPen>

QraphicsPolyLine::QraphicsPolyLine(const QVector<QPointF> &points,
                                   const QPen &pen,
                                   QGraphicsItem *parent) :
    QraphicsItem(parent),
    m_points(points),
    m_pen(pen)
{
    updateBBox();
    setAcceptHoverEvents(true);
}

QraphicsPolyLine::~QraphicsPolyLine()
{
}

QRectF QraphicsPolyLine::boundingRect() const
{
    return m_cachedBox.get();
}

void QraphicsPolyLine::updateBBox()
{
    if (m_cachedBox.needUpdate())
    {
        if (m_points.size() > 1)
        {
            QPointF leftTop(m_points[0]);
            QPointF bottomRight(m_points[0]);

            for (QPointF const &pos : m_points)
            {
                if (pos.x() > leftTop.x())
                    leftTop.setX(pos.x());
                else if (pos.x() < bottomRight.x())
                    bottomRight.setX(pos.x());
                if (pos.y() > leftTop.y())
                    leftTop.setY(pos.y());
                else if (pos.y() < bottomRight.y())
                    bottomRight.setY(pos.y());
            }
            QRectF box(leftTop, bottomRight);
            box = box.normalized();
            bbox::adjustBBox(m_pen, box);
            m_cachedBox.update(box);
        }
        else
            m_cachedBox.update(QRectF());
      }
}

void QraphicsPolyLine::addPoint(const QPointF &pos)
{
    prepareGeometryChange();
    m_points.append(pos);
    m_cachedBox.setModified();
}

void QraphicsPolyLine::removeLastPoint()
{
    if (m_points.empty() == false)
    {
        prepareGeometryChange();
        m_points.pop_back();
    }
}

void QraphicsPolyLine::setPen(const QPen &pen)
{
    prepareGeometryChange();
    m_pen = pen;
    m_cachedBox.setModified();
}

void QraphicsPolyLine::setLastPoint(const QPointF &pos)
{
    if (m_points.empty())
        return;
    prepareGeometryChange();
    m_points.back() = pos;
    m_cachedBox.setModified();
}

void QraphicsPolyLine::clearPoints()
{
    prepareGeometryChange();
    m_points.clear();
    m_cachedBox.setModified();
}

int QraphicsPolyLine::pointCount() const
{
    return m_points.count();
}

bool QraphicsPolyLine::empty() const
{
    return m_points.empty();
}

void QraphicsPolyLine::draw(QPainter *p, const QStyleOptionGraphicsItem *, QWidget *)
{
    if (m_points.size() > 0)
    {
        updateBBox();
        p->setPen(m_pen);
        p->drawPolyline(m_points);
    }
}

QPointF QraphicsPolyLine::pointAt(int i) const
{
    return m_points.at(i);
}

void QraphicsPolyLine::setPointAt(int i, QPointF const &pos)
{
    prepareGeometryChange();
    m_points[i] = pos;
    m_cachedBox.setModified();
}