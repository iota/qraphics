#ifndef QRAPHICSLINE_HPP
#define QRAPHICSLINE_HPP

#include "QraphicsItem.hpp"
#include "CachedValue.hpp"
#include <QPointF>
#include <QPen>

class QraphicsLine : public QraphicsItem
{
public:
    QraphicsLine(QLineF const &line, QPen const &pen, QGraphicsItem *parent = nullptr);
    QraphicsLine(QPen const &pen, QGraphicsItem *parent = nullptr);

    QRectF boundingRect() const;
    void setPen(QPen const &pen);
    void setLine(const QPointF &p0, const QPointF &p1);
    void setP0(const QPointF &p0);
    void setP1(const QPointF &p1);
    void draw(QPainter * p, const QStyleOptionGraphicsItem *, QWidget *);
public:
    int pointCount() const;
    QPointF pointAt(int i) const;
    void setPointAt(int i, const QPointF &pos);
private:
    void updateBBox();
private:
    QPointF             m_p0;
    QPointF             m_p1;
    QPen                m_pen;
    CachedValue<QRectF> m_cachedBox;
};

#endif // QRAPHICSLINE_HPP
