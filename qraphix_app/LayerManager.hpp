#ifndef QRAPHICSLAYERMANAGER_HPP
#define LAYERMANAGER_HPP
#include <QObject>

class QModelIndex;
class QItemSelectionModel;
class LayerModel;
class Layer;
class QraphicsScene;
class QraphicsItem;
class LayerWidget;

class LayerManager : public QObject
{
    Q_OBJECT
public:
    explicit LayerManager(QraphicsScene *scene);

    Layer       *currentLayer()const;
    int                 currentLayerIndex()const;
    int                 layerCount()const;
    Layer       *layerAt(int i)const;
    void                addItem(QraphicsItem *item, int layer);
    void                removeItem(QraphicsItem *item);
    LayerModel  *model()const;
    QItemSelectionModel *selectionModel()const;
    bool                hasCurrentLayer()const;
public slots:
    void    setCurrentLayer(int index);
    void    moveCurrentLayerUp();
    void    moveCurrentLayerDown();
    void    createLayer(QString const &name, int index);
    void    destroyLayer(int pos);
    void    destroyLayers();
private slots:
    void    currentChanged(QModelIndex const &current, QModelIndex const &old);
signals:
    void    currentLayerChanged(int layerIndex, int layerCount);
private:
    LayerModel  *m_model;
    QItemSelectionModel *m_selectionModel;
    Layer               *m_current;
    int                 m_currentIndex;
};

#endif // LAYERMANAGER_HPP
