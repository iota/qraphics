#include "QraphicsItem.hpp"
#include <QPainter>

static const qreal  boxMarginFactor = 1.2;
static const qreal  handleRadius = 4.0;
static const QPen   boxPen = QPen(Qt::gray);
static const QPen   handlePen = QPen(Qt::white);
static const QBrush handleBrush = QBrush(QColor("violet"));

namespace bbox
{
    void adjustBBox(QPen const & pen, QRectF &bbox)
    {
        qreal const hw = pen.width() / boxMarginFactor;
        bbox.adjust(-hw, -hw, hw, hw);
    }
}

QraphicsItem::QraphicsItem(QGraphicsItem *parent) :
    QGraphicsObject(parent),
    m_showBoxes(false),
    m_showHandles(false)
{
}

void QraphicsItem::paint(QPainter *p, const QStyleOptionGraphicsItem *style, QWidget *widget)
{
    draw(p, style, widget);
    if (m_showBoxes)
        drawItemBox(p);
    if (m_showHandles)
        drawItemHandles(p);
}

void QraphicsItem::setBoxesVisible(bool state)
{
    if (m_showBoxes != state)
    {
        m_showBoxes = state;
        update();
    }
}

void QraphicsItem::setHandlesVisible(bool state)
{
    if (m_showHandles != state)
    {
        m_showHandles = state;
        update();
    }
}

void   QraphicsItem::drawItemHandles(QPainter *p)const
{
    int const   count = pointCount();
    QPointF     pos;

    if (count == 0)
        return;
    p->setPen(handlePen);
    p->setBrush(handleBrush);
    for (int i = 0; i < count; ++i)
    {
        pos = pointAt(i);
        p->drawEllipse(pos, handleRadius, handleRadius);
    }
}

void   QraphicsItem::drawItemBox(QPainter *p)const
{
    p->setPen(boxPen);
    p->drawRect(boundingRect());
}

void    QraphicsItem::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    setHandlesVisible(true);
    QGraphicsObject::hoverEnterEvent(event);
}

void    QraphicsItem::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    setHandlesVisible(false);
    QGraphicsObject::hoverLeaveEvent(event);
}