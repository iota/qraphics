#include "ToolManager.hpp"
#include "ToolFactory.hpp"
#include "ToolModel.hpp"
#include "AbstractTool.hpp"
#include "QraphicsScene.hpp"
#include "PointerTool.hpp"
#include "LineTool.hpp"
#include "PolyLineTool.hpp"
#include <QGraphicsSceneMouseEvent>
#include <memory>

ToolManager::ToolManager(QraphicsScene *parent) :
    QObject(parent),
    m_scene(parent),
    m_toolFactory(nullptr),
    m_currentTool(nullptr)
{
    m_toolFactory = new ToolFactory(this);
    m_toolFactory->registerTool<PointerTool>(tr("Pointer"), QIcon(":/images/images/cursor-small.png"), TOOL_POINTER);
    m_toolFactory->registerTool<LineTool>(tr("Line"), QIcon(":/images/images/Pencil.png"), TOOL_LINE);
    m_toolFactory->registerTool<PolyLineTool>(tr("Polyline"), QIcon(":/images/images/TPen.png"), TOOL_PLINE);
}

ToolManager::~ToolManager()
{
    m_scene = nullptr;
}

ToolModel *ToolManager::model() const
{
    return m_toolFactory->getModel();
}

void ToolManager::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (m_currentTool)
        m_currentTool->mousePress(event, m_scene);
}

void ToolManager::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if (m_currentTool)
        m_currentTool->mouseRelease(event, m_scene);
}

void ToolManager::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if (m_currentTool)
        m_currentTool->mouseMove(event, m_scene);
}

void ToolManager::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    if (m_currentTool)
        m_currentTool->mouseDoubleClick(event, m_scene);
}

void ToolManager::changeTool(int index)
{
    setTool(m_toolFactory->create(index, m_scene));
}

void ToolManager::setTool(AbstractTool *tool)
{
    if (m_currentTool)
        m_currentTool->end(m_scene);
    m_currentTool.reset(tool);
    if (m_currentTool)
        m_currentTool->begin(m_scene);
}