#ifndef MAIN_WINDOW_HPP
#define MAIN_WINDOW_HPP

#include <QMainWindow>

class ActionModel;
class QListView;
class QraphicsView;
class QraphicsScene;
class LineWidget;
class LayerWidget;
class ToolWidget;
class QSettings;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void loadSettings(QSettings const &settings);
    void saveSettings(QSettings &settings)const;
protected:
    void closeEvent(QCloseEvent *event);
private:
    void    setupDockWidgets();
private:
    QraphicsView	*m_view;
    QraphicsScene	*m_scene;
    LineWidget      *m_penWidget;
    ToolWidget      *m_toolWidget;
    LayerWidget     *m_layerWidget;
};

#endif // MAIN_WINDOW_HPP
