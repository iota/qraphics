#include "LineWidget.hpp"
#include "ui_LineWidget.h"
#include <QPen>

LineWidget::LineWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LineWidget),
    m_pen(nullptr)
{
    ui->setupUi(this);
    m_styleMap[0] = Qt::SolidLine;
    m_styleMap[1] = Qt::DashLine;
    m_styleMap[2] = Qt::DotLine;
    m_styleMap[3] = Qt::CustomDashLine;
    m_capMap[0] = Qt::SquareCap;
    m_capMap[1] = Qt::FlatCap;
    m_capMap[2] = Qt::RoundCap;
    m_joinMap[0] = Qt::BevelJoin;
    m_joinMap[1] = Qt::MiterJoin;
    m_joinMap[2] = Qt::RoundJoin;
}

LineWidget::~LineWidget()
{
    delete ui;
}

void LineWidget::setPen(QPen const &pen)
{
    m_pen = pen;
    emit penChanged(m_pen);
}

void LineWidget::onSizeChanged(int value)
{
    m_pen.setWidth(value);
    emit penChanged(m_pen);
}

void LineWidget::onStyleChanged(int index)
{
    m_pen.setStyle(m_styleMap.value(index, Qt::SolidLine));
    emit penChanged(m_pen);
}

void LineWidget::onCapChanged(int index)
{
    m_pen.setCapStyle(m_capMap.value(index, Qt::SquareCap));
    emit penChanged(m_pen);
}

void LineWidget::onJoinChanged(int index)
{
    m_pen.setJoinStyle(m_joinMap.value(index, Qt::RoundJoin));
    emit penChanged(m_pen);
}