#ifndef ABSTRACTTOOL_HPP
#define ABSTRACTTOOL_HPP

class QraphicsScene;
class QGraphicsSceneMouseEvent;

/*! Définie l'interface d'un outil
 *
 *  Les méthodes begin() et end() sont appelées respectivement à l'activation
 *  et à la désactivation de l'outil. Ces méthodes sont l'endroit ou implémenter
 *  les constructions/initialisations des objets qui requière une QraphicsScene.
 *
 *  Les méthodes mouse<truc>() sont appelées en réponse au événements déclenché par l'utilisateur
 *  avec sa souris. Ces dans ces méthodes qu'on implémente la fonctionnalité de l'outil.
 *
 *  \see ToolFactory ToolModel ToolWidget
 */
class AbstractTool
{
public:
    virtual ~AbstractTool() = 0;

    virtual void mouseDoubleClick(QGraphicsSceneMouseEvent *event, QraphicsScene *scene) = 0;
    virtual void mousePress(QGraphicsSceneMouseEvent *event, QraphicsScene *scene) = 0;
    virtual void mouseRelease(QGraphicsSceneMouseEvent *event, QraphicsScene *scene) = 0;
    virtual void mouseMove(QGraphicsSceneMouseEvent *event, QraphicsScene *scene) = 0;

    virtual void begin(QraphicsScene *scene) = 0;
    virtual void end(QraphicsScene *scene) = 0;
};

#endif // ABSTRACTTOOL_HPP
