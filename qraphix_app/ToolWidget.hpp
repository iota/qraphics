#ifndef TOOLWIDGET_HPP
#define TOOLWIDGET_HPP
#include <QToolBar>

class ToolModel;
class QButtonGroup;

class ToolWidget : public QToolBar
{
    Q_OBJECT
public:
    explicit ToolWidget(QString const &title, QWidget *parent = 0);

    void setModel(ToolModel const *model);
signals:
    void    buttonClicked(int index);
private:
    QButtonGroup    *m_buttons;
};

#endif // TOOLWIDGET_HPP
