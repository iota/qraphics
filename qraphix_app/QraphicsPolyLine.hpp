#ifndef QRAPHICSPOLYLINE_HPP
#define QRAPHICSPOLYLINE_HPP
#include "QraphicsItem.hpp"
#include "CachedValue.hpp"
#include <QGraphicsItem>
#include <QPolygonF>
#include <QPen>
#include <QPointF>

class QPen;

class QraphicsPolyLine : public QraphicsItem
{
public:
    QraphicsPolyLine(QVector<QPointF> const &points,
                     const QPen &pen,
                     QGraphicsItem *parent = nullptr);
    ~QraphicsPolyLine();

    QRectF  boundingRect() const;
    void    addPoint(QPointF const &pos);
    void    removeLastPoint();
    void    setPen(QPen const &pen);
    void    setLastPoint(QPointF const &pos);
    void    clearPoints();
    int     pointCount()const;
    QPointF pointAt(int i) const;
    void    setPointAt(int i, const QPointF &pos);
    bool    empty()const;
    void    draw(QPainter *p, const QStyleOptionGraphicsItem *, QWidget *);
private:
    void    updateBBox();
private:
    QPolygonF           m_points;
    QPen                m_pen;
    CachedValue<QRectF> m_cachedBox;
};

#endif // QRAPHICSPOLYLINE_HPP
