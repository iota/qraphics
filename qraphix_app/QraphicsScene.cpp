#include "QraphicsScene.hpp"
#include "AbstractTool.hpp"
#include "LayerManager.hpp"
#include "LayerWidget.hpp"
#include "LineWidget.hpp"
#include "Layer.hpp"
#include "QraphicsLine.hpp"
#include "QraphicsPolyLine.hpp"
#include "QraphicsPolyLine.hpp"
#include "ToolManager.hpp"
#include "ToolWidget.hpp"
#include <QIcon>
#include <QItemSelectionModel>
#include <QString>

/*!
 * \class QraphicsScene
 * QraphicsScene est un raffinement de QGraphicsScene qui apporte
 * un système de calque pour l'organisation d'une scene.
 *
 * Seul QraphicsItem et ses dérivées peuvent être utilisé avec le système de
 * calque.
 */

QraphicsScene::QraphicsScene(QObject *parent) :
    QGraphicsScene(parent),
    m_toolManager(nullptr),
    m_layerManager(nullptr)
{
    m_toolManager = new ToolManager(this);
    m_layerManager = new LayerManager(this);
    m_toolManager->changeTool(ToolManager::TOOL_POINTER);
    m_layerManager->createLayer(tr("default layer"), 0);
    setCurrentPen(QPen(Qt::black, 0.0, Qt::SolidLine));
}

QraphicsScene::~QraphicsScene()
{
    m_layerManager->destroyLayers();
}

/*!
 * \brief Connecte les widgets de control à la scène.
 * \param toolWidget
 * \param penWidget
 * \param layerWidget
 */
void QraphicsScene::setup(ToolWidget *toolWidget,
                          LineWidget *penWidget,
                          LayerWidget *layerWidget)
{
    penWidget->setPen(m_currentPen);
    toolWidget->setModel(m_toolManager->model());
    layerWidget->setModel(m_layerManager->model());
    layerWidget->setSelectionModel(m_layerManager->selectionModel());
    connect(toolWidget, SIGNAL(buttonClicked(int)), this, SLOT(setTool(int)));
    connect(penWidget, SIGNAL(penChanged(QPen)), this, SLOT(setCurrentPen(QPen)));
    connect(layerWidget,SIGNAL(createLayer(QString)), this, SLOT(createNewLayer(QString)));
    connect(layerWidget,SIGNAL(destroyLayer()), this, SLOT(removeCurrentLayer()));
    connect(layerWidget,SIGNAL(moveLayerUp()), this, SLOT(moveCurrentLayerUp()));
    connect(layerWidget,SIGNAL(moveLayerDown()), this, SLOT(moveCurrentLayerDown()));
    connect(m_layerManager, SIGNAL(currentLayerChanged(int,int)), layerWidget, SLOT(selectionChanged(int,int)));
    setCurrentLayer(0);
}

void QraphicsScene::addQphxItem(QraphicsItem *item)
{
    int const   currentLayerIndex = m_layerManager->currentLayerIndex();

    if (item && currentLayerIndex >= 0)
    {
        m_layerManager->addItem(item, currentLayerIndex);
        update();
    }
}

void QraphicsScene::removeQphxItem(QraphicsItem *item)
{
    if (item)
    {
        m_layerManager->removeItem(item);
        update();
    }
}

void QraphicsScene::destroyQphxItem(QraphicsItem *item)
{
    if (item)
    {
        removeQphxItem(item);
        delete item;
    }
}

/*!
 * \brief Ajoute une ligne au calque courant.
 *
 * \param line
 * \param pen
 * \return L'adresse du nouvel item.
 */
QraphicsLine *QraphicsScene::addQphxLine(const QLineF &line, QPen const &pen)
{
    QraphicsLine    *new_line = new QraphicsLine(line, pen);

    addQphxItem(new_line);
    return new_line;
}

/*!
 * \brief Ajoute une ligne brisée au calque courant.
 *
 * \param points Liste des points qui détermines les lignes.
 * \param pen
 * \return L'adresse du nouvel item.
 */
QraphicsPolyLine *QraphicsScene::addQphxPolyLine(const QVector<QPointF> &points, QPen const &pen)
{
    QraphicsPolyLine    *new_pline = new QraphicsPolyLine(points, pen);

    addQphxItem(new_pline);
    return new_pline;
}

void QraphicsScene::setTool(int index)
{
    m_toolManager->changeTool(index);
}

void QraphicsScene::setCurrentPen(const QPen &pen)
{
    m_currentPen = pen;
    m_ghostPen = pen;
    m_ghostPen.setColor(Qt::blue);
}

const QPen &QraphicsScene::currentPen() const
{
    return m_currentPen;
}

const QPen &QraphicsScene::ghostPen() const
{
    return m_ghostPen;
}

void QraphicsScene::setCurrentLayer(int index)
{
    m_layerManager->setCurrentLayer(index);
}

void QraphicsScene::createNewLayer(const QString &name)
{
    m_layerManager->createLayer(name, m_layerManager->currentLayerIndex() + 1);
}

void QraphicsScene::removeCurrentLayer()
{
    m_layerManager->destroyLayer(m_layerManager->currentLayerIndex());
    update();
}

void QraphicsScene::moveCurrentLayerUp()
{
    m_layerManager->moveCurrentLayerUp();
    update();
}

void QraphicsScene::moveCurrentLayerDown()
{
    m_layerManager->moveCurrentLayerDown();
    update();
}

bool QraphicsScene::isCurrentLayerVisible() const
{
    Layer   *layer = m_layerManager->currentLayer();
    if (layer)
        return layer->isVisible();
    else
        return false;
}

void QraphicsScene::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (isCurrentLayerVisible())
        m_toolManager->mousePressEvent(event);
}

void QraphicsScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if (isCurrentLayerVisible())
        m_toolManager->mouseReleaseEvent(event);
}

void QraphicsScene::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if (isCurrentLayerVisible())
        m_toolManager->mouseMoveEvent(event);
}

void QraphicsScene::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    if (isCurrentLayerVisible())
        m_toolManager->mouseDoubleClickEvent(event);
}