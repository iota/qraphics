#include "ToolWidget.hpp"
#include "ToolModel.hpp"
#include <QButtonGroup>
#include <QToolButton>

ToolWidget::ToolWidget(const QString &title, QWidget *parent) :
    QToolBar(title, parent),
    m_buttons(nullptr)
{
    m_buttons = new QButtonGroup(this);
    m_buttons->setExclusive(true);
    setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    connect(m_buttons, SIGNAL(buttonClicked(int)), this, SIGNAL(buttonClicked(int)));
    setObjectName("tools bar");
    setIconSize(QSize(16, 16));
}

void ToolWidget::setModel(const ToolModel *model)
{
    QString     label;
    QIcon       icon;
    QToolButton *button;

    for (int i = 0;i < model->rowCount();++i)
    {
        label = qvariant_cast<QString>(model->data(model->index(i), Qt::DisplayRole));
        icon = qvariant_cast<QIcon>(model->data(model->index(i), Qt::DecorationRole));
        button = new QToolButton(this);
        button->setText(label);
        button->setIcon(icon);
        button->setCheckable(true);
        if (i == 0)
            button->setChecked(true);
        m_buttons->addButton(button, i);
        this->addWidget(button);
    }
}