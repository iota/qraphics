#include "QraphicsLine.hpp"
#include <QPainter>

QraphicsLine::QraphicsLine(const QLineF &line, const QPen &pen, QGraphicsItem *parent) :
    QraphicsItem(parent),
    m_p0(line.p1()),
    m_p1(line.p2()),
    m_pen(pen)
{
    updateBBox();
    setAcceptHoverEvents(true);
}

QRectF QraphicsLine::boundingRect() const
{
    return m_cachedBox.get();
}

void QraphicsLine::setPen(const QPen &pen)
{
    prepareGeometryChange();
    m_pen = pen;
    m_cachedBox.setModified();
}

void QraphicsLine::setLine(const QPointF &p0, const QPointF &p1)
{
    prepareGeometryChange();
    m_p0 = p0;
    m_p1 = p1;
    m_cachedBox.setModified();
}

void QraphicsLine::setP0(const QPointF &p0)
{
    prepareGeometryChange();
    m_p0 = p0;
    m_cachedBox.setModified();
}

void QraphicsLine::setP1(const QPointF &p1)
{
    prepareGeometryChange();
    m_p1 = p1;
    m_cachedBox.setModified();
}

void QraphicsLine::draw(QPainter *p, const QStyleOptionGraphicsItem *, QWidget *)
{
    updateBBox();
    p->setPen(m_pen);
    p->drawLine(m_p0, m_p1);
}

void QraphicsLine::setPointAt(int i, QPointF const &pos)
{
    prepareGeometryChange();
    if (i == 0)
        m_p0 = pos;
    else
        m_p1 = pos;
    m_cachedBox.setModified();
}

int QraphicsLine::pointCount() const
{
    return 2;
}

QPointF QraphicsLine::pointAt(int i) const
{
    if (i == 0)
        return m_p0;
    else
        return m_p1;
}

void QraphicsLine::updateBBox()
{
    if (m_cachedBox.needUpdate())
    {
        QRectF  box(m_p0, m_p1);

        box = box.normalized();
        bbox::adjustBBox(m_pen, box);
        m_cachedBox.update(box);
    }
}