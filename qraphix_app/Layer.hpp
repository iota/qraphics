#ifndef LAYER_HPP
#define LAYER_HPP
#include <QObject>
#include <QSet>

class QraphicsItem;
class QraphicsScene;
class QGraphicsItemGroup;

class Layer : public QObject
{
public:
    Layer(QString const &name,
                  QraphicsScene *scene,
                  int id,
                  QObject *parent);
    ~Layer();

    void            setName(QString const &name);
    void            setVisible(bool visible);
    void            setOrder(int order);
    int             getOrder()const;
    void            add(QraphicsItem *item);
    bool            remove(QraphicsItem *item);

    QString const   &name()const;
    bool            isVisible()const;
private:
    QString                 m_name;
    int                     m_id;
    QraphicsScene           *m_scene;
    QGraphicsItemGroup      *m_group;
    QSet<QraphicsItem *>    m_items;
};

#endif // LAYER_HPP
