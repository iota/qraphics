#ifndef LAYERWIDGET_HPP
#define LAYERWIDGET_HPP
#include <QWidget>

class QItemSelectionModel;
class QAction;

class LayerManager;
class QListView;
class LayerModel;
class QraphicsScene;

class LayerWidget : public QWidget
{
    Q_OBJECT
public:
    explicit LayerWidget(QWidget *parent = 0);

    void setModel(LayerModel *model);
    void setSelectionModel(QItemSelectionModel *selectionModel);
public slots:
    void selectionChanged(int layerIndex, int layerCount);
private slots:
    void createNewLayerImp();
private:
    void setupButtons(QWidget *widget);
signals:
    void createLayer(const QString &);
    void destroyLayer();
    void moveLayerUp();
    void moveLayerDown();
private:
    QListView       *m_view;
    QAction         *m_createNewLayer;
    QAction         *m_removeCurrentLayer;
    QAction         *m_moveUpCurrentLayer;
    QAction         *m_moveDownCurrentLayer;
};

#endif // LAYERWIDGET_HPP
