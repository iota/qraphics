#ifndef LAYOUTHELPER_HPP
#define LAYOUTHELPER_HPP
class QLayout;

namespace utils
{
    void    setupLayout(QLayout *layout);
}

#endif // LAYOUTHELPER_HPP
