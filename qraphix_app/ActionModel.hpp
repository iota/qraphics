#ifndef ACTION_MODEL_HPP
#define ACTION_MODEL_HPP

#include <QAbstractTableModel>
#include <QAction>
#include <QSharedPointer>
#include <QList>

class ActionModel : public QAbstractTableModel
{
    Q_OBJECT

    enum Columns : int
    {
        Label = 0,
        ColumnCount
    };

public:
    explicit ActionModel(QObject *parent = 0);

    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
public slots:
    void add(QAction *action);
private:
    typedef QList<QSharedPointer<QAction>> Actions;
    Actions m_actions;
};

#endif // ACTION_MODEL_HPP
